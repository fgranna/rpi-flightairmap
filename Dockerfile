FROM schoeffm/rpi-nginx-php5

MAINTAINER Frederik Granna

WORKDIR /usr/share/nginx

RUN apt-get update && \
    apt-get install -y php5-sqlite mysql-client vim pkg-config ca-certificates git-core inetutils-ping  --no-install-recommends && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

ENV commit_id caccdb691dcf16993c09cfb398567829330a9e67

RUN git clone https://github.com/Ysurac/FlightAirMap.git && \
    cd FlightAirMap && \
    git reset --hard $commit_id

RUN chown -R www-data /usr/share/nginx/FlightAirMap

COPY nginx_host.conf /etc/nginx/sites-enabled/nginx_host.conf

RUN rm /etc/nginx/sites-enabled/default

COPY settings.php /usr/share/nginx/FlightAirMap/require/settings.php

COPY startup.sh /opt/startup.sh

CMD /opt/startup.sh
